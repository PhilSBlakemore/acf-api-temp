$pb = jQuery.noConflict();

$pb(document).ready(function(){
    console.log('theme-functions.js loaded');
});

document.addEventListener('DOMContentLoaded', function() {
    var side_nav_elems = document.querySelectorAll('.sidenav');
    var side_nav_instances = M.Sidenav.init(side_nav_elems, {
        edge: 'left',
        draggable: true,
        inDuration: 250,
        outDuration: 200,
        onOpenStart: null,
        onOpenEnd: null,
        onCloseStart: null,
        onCloseEnd: null,
        preventScrolling: true
    });
});
