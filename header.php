<?php
/**
 * The header for our theme
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @package PhilBlakemore.com
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'philblakemore-com' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="navbar-fixed">
			<nav id="site-navigation" class="main-navigation">
				<div class="nav-wrapper">
					<?php the_custom_logo(); ?>
					<a href="#" data-target="slide-out" class="sidenav-trigger show-on-large right"><i class="material-icons">menu</i></a>
				</div>
			</nav>
		</div>
	</header>

<?php
	wp_nav_menu( array(
		'theme_location' 		=> 'desktop',
		'menu_id'        			=> 'slide-out',
		'menu_class'				=> 'sidenav',
	));
?>

	<div id="content" class="site-content">
