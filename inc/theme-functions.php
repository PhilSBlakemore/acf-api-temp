<?php
/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 * Priority 0 to make it available to lower priority callbacks.
 * @global int $content_width
 */
function philblakemore_com_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'philblakemore_com_content_width', 640 );
}
add_action( 'after_setup_theme', 'philblakemore_com_content_width', 0 );

/**
 * Add custom css to login page
 */
function theme_custom_login_css() {
    echo '<link rel="stylesheet" type="text/css" href=" ' . get_template_directory_uri() . '/login/login.css" />';
}
add_action( 'login_head', 'theme_custom_login_css' );

/**
 * Add custom elements to sidenav
 */
// add_filter( 'wp_nav_menu_items', 'theme_custom_menu_item', 10, 2 );
// function theme_custom_menu_item ( $items, $args ) {
//     if( ! is_search() && ! is_admin()) {
//         if ($args->theme_location == 'desktop') {
//             $nav_title = '<li><div class="user-view">
//                                 <a href="#user"><img src=""></a>
//                                 <a href="#company"><span class="company">philblakemore.com</span></a>
//                                 <a href="#name"><span class="name">Phil Blakemore</span></a>
//                                 <a href="#email"><span class="email">me@philblakemore.com</span></a>
//                                 </div>
//                                 </li>';
//             $social_nav = '<ul id="social">
//                         <li class="social-item"><a class="social-item-anchor" href="#"><i class="fab fa-twitter"></i></a></li>
//                         <li class="social-item"><a class="social-item-anchor" href="#"><i class="fab fa-instagram"></i></a></li>
//                         <li class="social-item"><a class="social-item-anchor" href="#"><i class="fab fa-linkedin-in"></i></a></li>
//                     </ul>';
//             $items = $nav_title . $items . $social_nav;
//         }
//         return $items;
//     }
// }

/**
 * Apply materializecss classes to custom logo
 */
add_filter( 'get_custom_logo', 'change_custom_logo_class' );
function change_custom_logo_class( $html ) {
    $html = str_replace( 'custom-logo-link', 'brand-logo', $html );
    return $html;
}

/**
 * Add svgs to media uploads
 */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
