<?php
function philblakemore_com_scripts() {
    wp_enqueue_style( 'philblakemore-com-style', get_stylesheet_uri() );
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Lato|Montserrat:700,900&display=swap' );
    wp_enqueue_style('material-fonts', 'https://fonts.googleapis.com/icon?family=Material+Icons' );

    wp_enqueue_script("jquery");
    wp_enqueue_script( 'materialize-js', get_template_directory_uri() . '/js/materialize.min.js', array(), null, true );
    wp_enqueue_script( 'flickity-js', get_template_directory_uri() . '/js/flickity.min.js', array(), null, true );
    wp_enqueue_script( 'isotope-js', get_template_directory_uri() . '/js/isotope.min.js', array(), null, true );
    wp_enqueue_script( 'greensock-js', get_template_directory_uri() . '/js/greensock/minified/TweenMax.min.js', array(), null, true );
    wp_enqueue_script( 'philblakemore-com-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
    wp_enqueue_script( 'theme-functions', get_template_directory_uri() . '/js/theme-functions.js', array('jquery'), null, true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'philblakemore_com_scripts' );
