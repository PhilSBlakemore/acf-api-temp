<?php
/**
 * PhilBlakemore.com functions and definitions
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 * @package PhilBlakemore.com
 */

if ( ! function_exists( 'philblakemore_com_setup' ) ) :
	function philblakemore_com_setup() {
		load_theme_textdomain( 'philblakemore-com', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		));

		add_theme_support( 'custom-background', apply_filters( 'philblakemore_com_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		)));

		/**
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		));

		register_nav_menus( array(
			'desktop' => esc_html__( 'Desktop', 'philblakemore-com' ),
			'mobile' => esc_html__( 'Mobile', 'philblakemore-com' ),
		));

	}
endif;
add_action( 'after_setup_theme', 'philblakemore_com_setup' );

/**
 * Implement the theme scripts
 */
require get_template_directory() . '/inc/theme-scripts.php';

/**
 * Implement the Theme widgets
 */
require get_template_directory() . '/inc/theme-widgets.php';

/**
 * Implement the theme functions
 */
require get_template_directory() . '/inc/theme-functions.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

