<?php
/**
 * The main template file for the homepage
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @package PhilBlakemore.com
 */
get_header();
?>

<div id="front-page" class="container">
    <main id="main" class="row">
        <div class="col s12">
            <?php
                if ( have_posts() ) :
                    while ( have_posts() ) : the_post();
                        get_template_part( 'template-parts/content', 'page' );
                    endwhile;
                    the_posts_navigation();
                else :
                    get_template_part( 'template-parts/content', 'none' );
                endif;
            ?>
        </div>
    </main>
</div>

<?php
get_footer();
