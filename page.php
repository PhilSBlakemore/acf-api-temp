<?php
/**
 * The template for displaying all pages
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @package PhilBlakemore.com
 */
get_header();
?>

<div id="primary" class="container">
	<main id="main" class="row">
		<div class="col s12 m9">
			<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', 'page' );
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			endwhile;
			?>
		</div>
		<div class="col s12 m3">
			<?php get_sidebar(); ?>
		</div>
	</main>
</div>

<?php
get_footer();
