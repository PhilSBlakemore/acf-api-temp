<?php
/**
 * The template for displaying the footer
 * Contains the closing of the #content div and all content after.
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package PhilBlakemore.com
 */
?>

	</div><!-- #content -->

	<footer class="page-footer" class="site-footer">
		<div class="container">
			<div class="row">
			</div>
		</div>
		<div class="footer-copyright">
			<div class="container">
				© <?php echo date('Y'); ?><span class="sep"> | </span>Copyright philblakemore.com
				<a class="grey-text text-lighten-4 right" href="#!">Additional Links</a>
			</div>
		</div>
	</footer>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
