<?php
/**
 * The main template file for the blog page
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @package PhilBlakemore.com
 */
get_header();
?>

<div id="blog" class="container">
    <main id="main" class="row">
        <div class="col s12 m9">
            <?php
            if ( have_posts() ) :
                while ( have_posts() ) : the_post();
                    get_template_part( 'template-parts/content', get_post_type() );
                endwhile;
                the_posts_navigation();
                else :
                get_template_part( 'template-parts/content', 'none' );
            endif;
            ?>
        </div>
        <div class="col s12 m3">
            <?php get_sidebar(); ?>
        </div>
    </main>
</div>

<?php
get_footer();
